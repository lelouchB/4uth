const projectList = document.querySelector(".guides");
const loggedOutLinks = document.querySelectorAll(".logged-out");
const loggedInLinks = document.querySelectorAll(".logged-in");
const accountDetails = document.querySelector(".account-details");

const setupUI = (user) => {
  if (user) {
    // account info
    db.collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => {
        const html = `
        <div>Logged in as ${user.email}
        </div>
        <div> ${doc.data().name}</div>
          `;
        accountDetails.innerHTML = html;
      });
    // toggle UI elements
    loggedInLinks.forEach((item) => (item.style.display = "block"));
    loggedOutLinks.forEach((item) => (item.style.display = "none"));
  } else {
    // hide account info
    accountDetails.innerHTML = "";

    // toggle UI elements
    loggedInLinks.forEach((item) => (item.style.display = "none"));
    loggedOutLinks.forEach((item) => (item.style.display = "block"));
  }
};

//set up project

const setupProjects = (data) => {
  let html = "";
  if (data.length) {
    data.forEach((doc) => {
      const project = doc.data();
      const li = `
        <li>
        <div class="collapsible-header grey lighten-4">${project.Title}</div>
        <div class="collapsible-body">${project.content}</div>
        </li>
        `;
      html += li;
    });
    projectList.innerHTML = html;
  } else {
    projectList.innerHTML = `<h5 class='center-align'>Login to view Projects</h5>`;
  }
};

// setup materialize components
document.addEventListener("DOMContentLoaded", () => {
  var modals = document.querySelectorAll(".modal");
  M.Modal.init(modals);

  var items = document.querySelectorAll(".collapsible");
  M.Collapsible.init(items);
});
