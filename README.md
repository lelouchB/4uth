# 4uth

## Steps to use

1.  Download or clone the repo.
2.  Replace firebaseConfig eith your project credentials.
3.  Open index.html

## Features

1. SignUp functionality which sends user email verification after succesfull signup.
2. Login functionality which allows the user to login and view some restricted data.
3. Password Reset, in case some users forget their passwords.

## What it does
** It is a boilerplate which you can just download or clone and integrate in your app. The integration is pretty simple and you just have to add your personal firebase project config and nothing else and it is ready to go. **

## How I built it
I wanted it to be used by anyone from Reactjs developers to complete beginners so I kept the tools simple and just made it with HTML, CSS and javascript, nothing too fancy. used a few API like firebase and material-ui. I then used GitHub-buttons to add download and start the option of the original repo on Github.

## Challenges I ran into
Well figuring out how to verify users by sending them verification email was very challenging for me.

## Accomplishments that I'm proud of
_ I am really proud that this boilerplate will not only help me but many other people in the future. _

## What I learned
Different Firebase methods and their uses. How to buy Domains, connect them to our website and different new technologies like DNS, SSL etc.

## What's next for Signup/Login Boilerplate
I plan to make the same infrastructure using different stacks and technologies like MERN, express, MEAN, MEVN, Nodejs, passport.js etc. so that a person can choose.

## Built With
api
css3
firebase
html5
javascript
material-ui
## Try it out

[live demo](https://4uth.online/)
