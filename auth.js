// listen for auth changes
firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    // User is signed in.
    if (user.emailVerified) {
      setupUI(user);
      //data
      db.collection("projects").onSnapshot(
        (snapshot) => {
          setupProjects(snapshot.docs);
        },
        (err) => {
          console.log(err.message);
        }
      );
    } else {
      // User is signed out.
      setupUI();
      setupProjects([]);
    }
  } else {
    // User is signed out.
    setupUI();
    setupProjects([]);
  }
});

//singup
const signupForm = document.querySelector("#signup-form");
signupForm.addEventListener("submit", (e) => {
  e.preventDefault();

  //get user info
  const email = signupForm["signup-email"].value;
  const password = signupForm["signup-password"].value;
  //const credentials;
  // signup user
  auth
    .createUserWithEmailAndPassword(email, password)
    .then((cred) => {
      return db.collection("users").doc(cred.user.uid).set({
        name: signupForm["signup-name"].value,
      });
    })
    .then(() => {
      auth.currentUser
        .sendEmailVerification()
        .then(() => {
          auth.signOut();
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .then(() => {
      const modal = document.querySelector("#modal-signup");
      M.Modal.getInstance(modal).close();
      signupForm.reset();
      signupForm.querySelector(".error").innerHTML = "";
    })
    .catch((err) => {
      signupForm.querySelector(".error").innerHTML = err.message;
    });
});

// logout
const logout = document.querySelector("#logout");

logout.addEventListener("click", (e) => {
  e.preventDefault();
  auth.signOut();
});

//login
const loginForm = document.querySelector("#login-form");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  //get user info
  const email = loginForm["login-email"].value;
  const password = loginForm["login-password"].value;

  auth
    .signInWithEmailAndPassword(email, password)
    .then((cred) => {
      const modal = document.querySelector("#modal-login");
      M.Modal.getInstance(modal).close();
      loginForm.reset();
      loginForm.querySelector(".error").innerHTML = "";
    })
    .catch((err) => {
      loginForm.querySelector(".error").innerHTML = err.message;
    });
});

//reset

const resetForm = document.querySelector("#reset-form");

resetForm.addEventListener("submit", (e) => {
  e.preventDefault();

  //get user info
  const email = resetForm["reset-email"].value;
  auth
    .sendPasswordResetEmail(email)
    .then((cred) => {
      const modal = document.querySelector("#modal-reset");
      M.Modal.getInstance(modal).close();
      resetForm.reset();
      resetForm.querySelector(".error").innerHTML = "";
    })
    .catch((err) => {
      resetForm.querySelector(".error").innerHTML = err.message;
    });
});
